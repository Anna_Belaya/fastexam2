package gys.pro.fastexam.service.mongo;

import gys.pro.fastexam.AbstractUnitTest;
import gys.pro.fastexam.domain.Expert;
import gys.pro.fastexam.repository.ExpertRepository;
import gys.pro.fastexam.service.ExpertService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

/**
 * @author Yegor Gulimov
 */
public class ExpertMongoServiceUnitTest extends AbstractUnitTest {

    @Autowired
    private ExpertMongoService expertMongoService;

    @Mock
    private ExpertRepository expertRepository;

    @Mock
    private Expert expert;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        expertMongoService.setExpertRepository(expertRepository);
    }

    @Test
    public void testGetByExistedId() {
        when(expertRepository.findOne(anyString())).thenReturn(expert);

        Expert receivedExpert = expertMongoService.get("TestId");

        verify(expertRepository).findOne("TestId");

        assertThat(receivedExpert, is(expert));
    }

    @Test
    public void testGetByNotExistedId() {
        when(expertRepository.findOne(anyString())).thenReturn(null);

        Expert receivedExpert = expertMongoService.get("TestId");

        verify(expertRepository).findOne("TestId");

        assertThat(receivedExpert, is(nullValue()));
    }

    @Test
    public void testUpdateWithExistedIdAndCorrectExpert() {
        Expert expert2 = new Expert();
        expert2.setLogin("UpdatedLogin");
        expert2.setId("NoTestId");

        when(expertRepository.findOne(anyString())).thenReturn(expert);
        when(expert.getLogin()).thenReturn("TestLogin");
        when(expertRepository.findExpertByLogin("UpdatedLogin")).thenReturn(null);
        when(expertRepository.save(expert2)).thenReturn(expert2);

        Expert receivedUpdatedExpert = expertMongoService.update("TestId", expert2);

        verify(expertRepository).findOne("TestId");
        verify(expertRepository).findExpertByLogin("UpdatedLogin");
        verify(expertRepository).save(expert2);

        assertThat(receivedUpdatedExpert, is(expert2));
        assertThat(receivedUpdatedExpert.getId(), is("TestId"));
        assertThat(receivedUpdatedExpert.getLogin(), is("UpdatedLogin"));

    }

    @Test
    public void testUpdateWithNotExistedId() {
        when(expertRepository.findOne(anyString())).thenReturn(null);

        Expert receivedUpdatedExpert = expertMongoService.update("TestId", expert);

        verify(expertRepository).findOne("TestId");

        assertThat(receivedUpdatedExpert, is(nullValue()));
    }

    @Test
    public void testUpdateWithConflictLogin() {
        Expert expertActual = new Expert("Actual", "0");
        Expert updated = new Expert("Updated", "0");
        Expert expertChecked = new Expert("Updated", "0");

        when(expertRepository.findOne(anyString())).thenReturn(expertActual);
        when(expertRepository.findExpertByLogin(anyString())).thenReturn(expertChecked);

        Expert receivedUpdatedExpert = expertMongoService.update("TestId", updated);

        verify(expertRepository).findOne("TestId");
        verify(expertRepository).findExpertByLogin("Updated");

        assertThat(receivedUpdatedExpert, is(nullValue()));
    }

    @Test
    public void testUpdateWithSameLogin() {
        Expert actual = new Expert("Actual", "0");
        actual.setId("ActualId");
        Expert updated = new Expert("Actual", "1");

        when(expertRepository.findOne("ActualId")).thenReturn(actual);
        when(expertRepository.findExpertByLogin("Actual")).thenReturn(actual);
        when(expertRepository.save(updated)).thenReturn(updated);

        Expert receivedUpdatedExpert = expertMongoService.update(actual.getId(), updated);
        verify(expertRepository).findOne("ActualId");
        verify(expertRepository).findExpertByLogin("Actual");
        verify(expertRepository).save(updated);

        assertThat(receivedUpdatedExpert, is(updated));
        assertThat(receivedUpdatedExpert.getId(), is("ActualId"));
        assertThat(receivedUpdatedExpert.getLogin(), is("Actual"));
        assertThat(receivedUpdatedExpert.getPassword(), is("1"));
    }

    @Test
    public void testCreateWithUniqueLogin() {
        when(expert.getLogin()).thenReturn("TestLogin");
        when(expertRepository.findExpertByLogin(anyString())).thenReturn(null);
        when(expertRepository.insert(expert)).thenReturn(expert);

        Expert createdExpert = expertMongoService.create(expert);

        verify(expertRepository).findExpertByLogin("TestLogin");
        verify(expertRepository).insert(expert);

        assertThat(createdExpert, is(expert));
    }

    @Test
    public void testCreateWithDuplicateLogin() {
        when(expertRepository.findExpertByLogin("TestLogin")).thenReturn(expert);

        Expert createdExpert = expertMongoService.create(new Expert("TestLogin", "0"));

        verify(expertRepository).findExpertByLogin("TestLogin");

        assertThat(createdExpert, is(nullValue()));
    }

    @Test
    public void testDeleteExistedExpert() {
        when(expertRepository.exists("TestId")).thenReturn(false);

        boolean deletingResult = expertMongoService.delete("TestId");

        verify(expertRepository).delete("TestId");

        assertThat(deletingResult, is(true));
    }

    @Test
    public void testDeleteNotExistedExpert() {
        when(expertRepository.exists("TestId")).thenReturn(true);

        boolean deletingResult = expertMongoService.delete("TestId");

        verify(expertRepository).delete("TestId");

        assertThat(deletingResult, is(false));
    }

    @Test
    public void testGetByExistedLogin() {
        when(expertRepository.findExpertByLogin("Login")).thenReturn(expert);

        Expert receivedExpert = expertMongoService.getByLogin("Login");

        verify(expertRepository).findExpertByLogin("Login");

        assertThat(receivedExpert, is(expert));
    }

    @Test
    public void testGetByNotExistedLogin() {
        when(expertRepository.findExpertByLogin("Login")).thenReturn(null);

        Expert receivedExpert = expertMongoService.getByLogin("Login");

        verify(expertRepository).findExpertByLogin("Login");

        assertThat(receivedExpert, is(nullValue()));
    }

    @Test
    public void testGetAll() {
        List<Expert> experts = Arrays.asList(expert);

        when(expertRepository.findAll()).thenReturn(experts);

        List<Expert> expertsReceived = expertMongoService.list();

        verify(expertRepository).findAll();

        assertThat(expertsReceived, is(experts));
    }

    @Test
    public void testClear() {
        expertMongoService.clear();

        verify(expertRepository).deleteAll();
    }
}
