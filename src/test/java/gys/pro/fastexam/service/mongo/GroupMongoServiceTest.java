package gys.pro.fastexam.service.mongo;

import gys.pro.fastexam.AbstractUnitTest;
import gys.pro.fastexam.domain.Expert;
import gys.pro.fastexam.domain.Group;
import gys.pro.fastexam.service.ExpertService;
import gys.pro.fastexam.service.GroupService;
import gys.pro.fastexam.service.StudentService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class GroupMongoServiceTest extends AbstractUnitTest {

    @Autowired
    private GroupService groupService;

    @Autowired
    private ExpertService expertService;

    @Autowired
    private StudentService studentService;

    @Before
    public void init() {
        groupService.clear();
        expertService.clear();
        studentService.clear();
    }

    @Test
    public void testCreateGroupWithUniqueName() {
        Group group = new Group("Test");
        Group created = groupService.create(group);

        assertThat(created, is(group));
        assertThat(groupService.list(), hasItem(created));
        assertThat(groupService.list().size(), is(1));
    }

    @Test
    public void testCreateGroupWithNotUniqueName() {
        Group group1 = groupService.create(new Group("Test"));
        Group group2 = groupService.create(new Group("Test"));

        assertThat(groupService.list().size(), is(1));
        assertThat(group1, is(notNullValue()));
        assertThat(group2, is(nullValue()));
        assertThat(groupService.list(), hasItem(group1));
        assertThat(groupService.list(), not(hasItem(group2)));
    }

    @Test
    public void testGetGroupByExistedId() {
        Group group = groupService.create(new Group());

        Group receivedGroup = groupService.get(group.getId());

        assertThat(receivedGroup, is(group));
    }

    @Test
    public void testGetGroupByNotExistedId() {
        Group receivedGroup = groupService.get("Test");

        assertThat(receivedGroup, is(nullValue()));
    }

    @Test
    public void testGetGroupByExistedName() {
        Group group = groupService.create(new Group("Test"));

        Group receivedGroup = groupService.getByName("Test");

        assertThat(receivedGroup, is(group));
    }

    @Test
    public void testGetGroupByNotExistedName() {
        Group receivedGroup = groupService.getByName("Test");

        assertThat(receivedGroup, is(nullValue()));
    }

    @Test
    public void testUpdateGroupWithCorrectIdAndGroup() {
        Group existedGroup = groupService.create(new Group("Test-1"));
        Group updatedGroup = new Group("Test-2");

        Group receivedUpdatedGroup = groupService.update(existedGroup.getId(), updatedGroup);

        assertThat(receivedUpdatedGroup, is(existedGroup));
        assertThat(receivedUpdatedGroup.getId(), is(existedGroup.getId()));
        assertThat(receivedUpdatedGroup.getName(), is("Test-2"));
        assertThat(groupService.list().size(), is(1));
    }

    @Test
    public void testUpdateGroupWithNotExistedId() {
        Group receivedUpdatedGroup = groupService.update("not existed id", new Group());

        assertThat(receivedUpdatedGroup, is(nullValue()));
    }

    @Test
    public void testUpdateGroupWithExistedName() {
        Group existedGroup = groupService.create(new Group("Test-1"));
        Group otherGroup = groupService.create(new Group("Test-2"));
        Group updatedGroup = new Group("Test-2");

        Group receivedUpdatedGroup = groupService.update(existedGroup.getId(), updatedGroup);

        assertThat(receivedUpdatedGroup, is(nullValue()));
    }

    @Test
    public void testUpdateGroupWithSameName() {
        Group existedGroup = groupService.create(new Group("Test-1"));
        Group updatedGroup = new Group("Test-1");

        Group receivedUpdatedGroup = groupService.update(existedGroup.getId(), updatedGroup);

        assertThat(receivedUpdatedGroup, is(existedGroup));
        assertThat(receivedUpdatedGroup.getId(), is(existedGroup.getId()));
    }

    @Test
    public void testDeleteGroupByExistedId() {
        Group existedGroup = groupService.create(new Group("Test"));

        boolean deletingResult = groupService.delete(existedGroup.getId());

        assertThat(deletingResult, is(true));
    }

    @Test
    public void testDeleteGroupByNotExistedId() {
        boolean deletingResult = groupService.delete("not existed id");

        assertThat(deletingResult, is(false));
    }

    @Test
    public void testList() {
        Group group1 = groupService.create(new Group("Test-1"));
        Group group2 = groupService.create(new Group("Test-2"));

        assertThat(groupService.list(), hasItems(group1, group2));
        assertThat(groupService.list().size(), is(2));
    }

    @Test
    public void testClear() {
        Group group1 = groupService.create(new Group("Test-1"));
        Group group2 = groupService.create(new Group("Test-2"));

        assertThat(groupService.list(), hasItems(group1, group2));
        assertThat(groupService.list().size(), is(2));

        groupService.clear();

        assertThat(groupService.list(), is(Collections.<Group>emptyList()));
    }

    @Test
    public void testListByExistedExpert() {
        Expert expert = expertService.create(new Expert("TestExpert", "0"));
        Group group1 = groupService.create(new Group("TestGroup-1"));
        Group group2 = groupService.create(new Group("TestGroup-2"));
        Group group3 = groupService.create(new Group("TestGroup-3"));

        group1 = groupService.addExpertToGroup(group1.getId(), expert.getId());
        group1 = groupService.addExpertToGroup(group2.getId(), expert.getId());

        List<Group> receivedExpertGroups = groupService.listByExpert(expert.getId());

        assertThat(receivedExpertGroups.size(), is(2));
        assertThat(receivedExpertGroups, hasItems(group1, group2));
        assertThat(receivedExpertGroups, not(hasItem(group3)));
    }

}