package gys.pro.fastexam.service.mongo;

import gys.pro.fastexam.AbstractUnitTest;
import gys.pro.fastexam.domain.Expert;
import gys.pro.fastexam.service.ExpertService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class ExpertMongoServiceTest extends AbstractUnitTest {

    @Autowired
    private ExpertService expertService;

    @Before
    public void init() {
        expertService.clear();
    }

    @Test
    public void testGetWithNotExistedId() {
        assertNull(expertService.get("Test"));
    }

    @Test
    public void testGetWithExistedId() {
        Expert expert = expertService.create(new Expert("TestLogin", "TestPassword"));

        expert = expertService.get(expert.getId());

        assertThat(expert.getId(), is(notNullValue()));
        assertThat(expert.getLogin(), is("TestLogin"));
        assertThat(expert.getPassword(), is("TestPassword"));
    }

    @Test
    public void testCreate() {
        assertThat(expertService.list(), is(Collections.<Expert>emptyList()));

        Expert expert = expertService.create(new Expert("TestLogin", "TestPassword"));

        assertThat(expertService.list().size(), is(1));
        assertThat(expertService.list(), hasItem(expert));
        assertThat(expertService.get(expert.getId()), is(expert));
    }



}