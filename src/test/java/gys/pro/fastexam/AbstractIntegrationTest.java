package gys.pro.fastexam;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.RestAssured;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Yegor Gulimov
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {ApplicationTestConfig.class})
@WebAppConfiguration
@IntegrationTest("server.port:0")
@DirtiesContext
public abstract class AbstractIntegrationTest {
    @Value("${local.server.port}")
    protected int port;

    protected ObjectMapper mapper = new ObjectMapper();

    @Before
    public void init() {
        RestAssured.port = port;
    }
}
