package gys.pro.fastexam.controller.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import gys.pro.fastexam.AbstractIntegrationTest;
import gys.pro.fastexam.domain.Expert;
import gys.pro.fastexam.domain.dto.ExpertDTO;
import gys.pro.fastexam.service.ExpertService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.*;
import static org.apache.http.HttpStatus.*;


public class ExpertControllerRestTest extends AbstractIntegrationTest {

    @Override
    public void init() {
        super.init();
        expertService.clear();
    }

    @Autowired
    private ExpertService expertService;

    @Test
    public void testGetExpertByLoginWithNotExistedLogin() {
        given()
                .contentType(JSON)
        .when()
                .get("/rest/experts/login/Test")
        .then()
                .statusCode(SC_NO_CONTENT);
    }

    @Test
    public void testGetExpertByLoginWithExistedLogin() throws JsonProcessingException {
        Expert expert = expertService.create(new Expert("Test", "0"));
        String expertJson = mapper.writeValueAsString(expert);

        given()
                .contentType(JSON)
        .when()
                .get("/rest/experts/login/Test")
        .then()
                .statusCode(SC_OK)
                .body(is(expertJson))
                .body("id", is(expert.getId()))
                .body("login", is("Test"))
                .body("password", is("0"));
    }

    @Test
    public void testGetExpertDTOByLoginWithNotExistedLogin() {
        given()
                .contentType(JSON)
        .when()
                .get("/rest/experts/login/Test/dto")
        .then()
                .statusCode(SC_NO_CONTENT);
    }

    @Test
    public void testGetExpertDTOByLoginWithExistedLogin() throws JsonProcessingException {
        Expert expert = expertService.create(new Expert("Test", "0"));
        ExpertDTO expertDTO = new ExpertDTO(expert);
        String expertDTOJson = mapper.writeValueAsString(expertDTO);

        given()
                .contentType(JSON)
                .when()
                .get("/rest/experts/login/Test/dto")
                .then()
                .statusCode(SC_OK)
                .body(is(expertDTOJson))
                .body("id", is(expert.getId()))
                .body("login", is("Test"));
    }

}