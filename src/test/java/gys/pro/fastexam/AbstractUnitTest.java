package gys.pro.fastexam;

import gys.pro.fastexam.ApplicationTestConfig;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author Yegor Gulimov
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {ApplicationTestConfig.class})
@WebAppConfiguration
@DirtiesContext
public abstract class AbstractUnitTest {

}
