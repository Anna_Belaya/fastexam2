package gys.pro.fastexam.domain;

import gys.pro.fastexam.AbstractUnitTest;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class GroupTest extends AbstractUnitTest {

    @Test
    public void testCreateCroup() {
        Group group = new Group("Test");

        assertThat(group, is(notNullValue()));
        assertThat(group.getId(), is(nullValue()));
        assertThat(group.getName(), is("Test"));
        assertThat(group.getExperts(), is(Collections.<Expert>emptyList()));
        assertThat(group.getStudents(), is(Collections.<Student>emptyList()));
    }

    @Test
    public void testToString() {
        Group group = new Group("Test");

        assertThat(group.toString(), is("Test"));
    }

    @Test
    public void testEqualsAndHashCode() {
        Group group1 = new Group("Test-1");
        Group group2 = new Group("Test-2");
        Group group3 = new Group("Test-1");
        group1.setId("a");
        group2.setId("a");
        group3.setId("b");

        //equals
        assertThat(group1, is(equalTo(group1)));
        assertThat(group1, is(equalTo(group2)));
        assertThat(group1, is(not(equalTo(group3))));
        assertThat(group1, is(not(equalTo(null))));

        //hash code
        assertEquals(group1.hashCode(), group1.hashCode());
        assertEquals(group1.hashCode(), group2.hashCode());
        assertNotEquals(group1.hashCode(), group3.hashCode());
    }

}