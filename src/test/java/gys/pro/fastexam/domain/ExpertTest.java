package gys.pro.fastexam.domain;

import gys.pro.fastexam.AbstractUnitTest;
import gys.pro.fastexam.ApplicationTestConfig;


import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.SpringApplicationConfiguration;

import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class ExpertTest extends AbstractUnitTest {

    @Test
    public void testCreatingExpert() {
        Expert expert = new Expert("TestLogin", "TestPassword");

        assertThat(expert.getId(), is(nullValue()));
        assertThat(expert.getLogin(), is("TestLogin"));
        assertThat(expert.getPassword(), is("TestPassword"));
    }

    @Test
    public void testCompareToExpert() {
        Expert[] experts = new Expert[]{new Expert(), new Expert(), new Expert()};
        experts[0].setId("0");
        experts[1].setId("1");
        experts[2].setId("0");

        assertThat(experts[0].equals(experts[1]), is(false));
        assertThat(experts[0].equals(experts[2]), is(true));

    }
}