$(document).ready(function() {
    var login = $("#userPrincipal").val();
    $.ajax({
        type: "GET",
        url: "/rest/experts/login/" + login + "/dto",
        success: function(result) {
            var expert = JSON.stringify(result);
            console.log("JSON: " + expert);
            expert = JSON.parse(expert);

            $("#expertFullName").text(expert.login);
        }
    });
});