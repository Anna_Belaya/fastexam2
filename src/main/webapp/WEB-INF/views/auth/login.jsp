<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Log in</title>
</head>
<body>

<c:url value="/web/login" var="loginUrl"/>



<div class="container" style="width: 40%">

    <form class="form-signin" action="${loginUrl}" method="post">

        <c:if test="${param.error != null}">
            <p>
                Invalid username and password.
            </p>
        </c:if>
        <c:if test="${param.logout != null}">
            <p>
                You have been logged out.
            </p>
        </c:if>

        <h2 class="form-signin-heading" style="text-align: center">Please log in</h2>

        <label for="username" class="sr-only">Username</label>
        <input type="text" id="username" name="username" class="form-control" placeholder="login" required autofocus/>

        <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control" placeholder="password" required/>

        <input type="hidden"
               name="${_csrf.parameterName}"
               value="${_csrf.token}"/>

        <button class="btn btn-lg btn-primary btn-block" type="submit">log in</button>
    </form>

</div> <!-- /container -->

</body>
</html>