package gys.pro.fastexam.domain;

import org.springframework.data.annotation.Id;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Yegor Gulimov
 */
public class Student {

    @Id
    private String id;
    private String lastName;
    private String firstName;
    private Date examDate;

    public Student() { }

    public Student(String lastName, String firstName, Date examDate) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.examDate = examDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy H:mm");
        if (examDate == null) {
            return String.format("%s %s : undefined", lastName, firstName);
        }
        return String.format("%s %s : %s", lastName, firstName, sdf.format(examDate));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;

        Student student = (Student) o;

        if (!id.equals(student.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
