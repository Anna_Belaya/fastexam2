package gys.pro.fastexam.domain.dto;

import gys.pro.fastexam.domain.Expert;

/**
 * @author Yegor Gulimov
 */
public class ExpertDTO implements Comparable<ExpertDTO>{
    private String id;
    private String login;

    public ExpertDTO(Expert expert) {
        id = expert.getId();
        login = expert.getLogin();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public int compareTo(ExpertDTO o) {
        return login.toLowerCase().compareTo(o.login.toLowerCase());
    }
}
