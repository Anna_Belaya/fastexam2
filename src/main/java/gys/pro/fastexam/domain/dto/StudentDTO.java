package gys.pro.fastexam.domain.dto;

import gys.pro.fastexam.domain.Student;

import java.util.Date;

/**
 * @author Yegor Gulimov
 */
public class StudentDTO implements Comparable<StudentDTO> {
    private String id;
    private String fullName;
    private Date examDate;
    private String examId;

    public StudentDTO(Student student, String examId) {
        id = student.getId();
        fullName = String.format("%s %s", student.getLastName(), student.getFirstName());
        examDate = student.getExamDate();
        this.examId = examId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getExamDate() {
        return examDate;
    }

    public void setExamDate(Date examDate) {
        this.examDate = examDate;
    }

    public String getExamId() {
        return examId;
    }

    public void setExamId(String examId) {
        this.examId = examId;
    }

    @Override
    public int compareTo(StudentDTO o) {
        if (examDate == null) {
            return 1;
        }
        if (o.examDate == null) {
            return -1;
        }
        return examDate.before(o.examDate) ? -1 : 1;
    }
}
