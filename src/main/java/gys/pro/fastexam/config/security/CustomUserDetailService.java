package gys.pro.fastexam.config.security;

import gys.pro.fastexam.domain.Expert;
import gys.pro.fastexam.repository.ExpertRepository;
import gys.pro.fastexam.service.ExpertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * @author Yegor Gulimov
 *
 * service for security
 *
 */

@Component
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private ExpertService expertService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Expert expert = expertService.getByLogin(s);

        if (expert == null) {
            throw new UsernameNotFoundException("Username " + s + " not found");
        }

        return new ExpertSecurity(expert);
    }
}

