package gys.pro.fastexam.util;

import gys.pro.fastexam.domain.Expert;
import gys.pro.fastexam.domain.Group;
import gys.pro.fastexam.domain.Student;
import gys.pro.fastexam.repository.ExpertRepository;
import gys.pro.fastexam.service.ExpertService;
import gys.pro.fastexam.service.GroupService;
import gys.pro.fastexam.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author Yegor Gulimov
 */
@Component
public class MongoTool {

    @Autowired
    private ExpertService expertService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private GroupService groupService;

    @PostConstruct
    public void init() throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy H:mm");

        //Expert expert = expertService.create(new Expert("GYS2", "0000"));
        //System.err.println("EXPERT: " + expert);

        //Student student = studentService.create(new Student("Reed", "Elliot", sdf.parse("14.04.2016 11:00")));
        //System.err.println("STUDENT: " + student);
        //Student student = studentService.list().get(0);
        //studentService.create(student);
        //Expert expert = expertService.getByLogin("GYS-2");
        //expertService.update(expert.getId(), new Expert("GYS", "0000"));

        //Student student = studentService.create(new Student());
        //System.err.println("DELETING STUDENT: " + studentService.delete(student.getId()));

        //Expert expert = expertService.create(new Expert());
        //System.err.println("DELETING EXPERT: " + expertService.delete(expert.getId()));


        //studentService.update(student.getId(), new Student("Cox", "Perry", sdf.parse("14.04.2016 10:30")));

        //groupService.create(new Group("Java-1"));
        //groupService.create(new Group("Java-3"));


        /*
        List<Group> groups = groupService.list();
        System.err.println("ALL GROUPS:" + groups);
        List<Student> students = studentService.list();
        System.err.println("ALL STUDENTS: " + students);
        List<Expert> experts = expertService.list();
        System.err.println("ALL EXPERTS: " + experts);

        //groupService.removeExpertFromGroup(groups.get(0).getId(), experts.get(1).getId());
        System.err.println("GR EX: " + groups.get(0).getExperts());

        groups = groupService.getByExpert(experts.get(0).getId());
        System.err.println("GROUPS BY EXPERT: " + groups);
        */
        //Group group = groupService.list().get(0);
        //group = groupService.update(group.getId(), new Group(group.getName()));
        //System.err.println("GR: " + group);

        Group group = groupService.getByName("Java-o");
        System.err.println("GR: " + group);
    }
}
