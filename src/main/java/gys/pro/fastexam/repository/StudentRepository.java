package gys.pro.fastexam.repository;

import gys.pro.fastexam.domain.Student;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Yegor Gulimov
 */
public interface StudentRepository extends MongoRepository<Student, String> {
}
