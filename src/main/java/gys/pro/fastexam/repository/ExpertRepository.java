package gys.pro.fastexam.repository;

import gys.pro.fastexam.domain.Expert;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Yegor Gulimov
 */
public interface ExpertRepository extends MongoRepository<Expert, String> {
    Expert findExpertByLogin(String login);
}
