package gys.pro.fastexam.repository;

import gys.pro.fastexam.domain.Group;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

/**
 * @author Yegor Gulimov
 */
public interface GroupRepository extends MongoRepository<Group, String> {
    Group findGroupByName(String name);
    List<Group> findByExpertsId(String expertId);
}
