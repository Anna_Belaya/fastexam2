package gys.pro.fastexam.controller.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Web controller for all web-related requests
 *
 * @author Yegor Gulimov
 */

@Controller
@RequestMapping("/web")
public class WebController {

    @RequestMapping("/login")
    public ModelAndView login() {
        return new ModelAndView("auth/login");
    }

    @RequestMapping("/expert")
    public ModelAndView expert() {
        return new ModelAndView("profile/expert");
    }
}
