package gys.pro.fastexam.controller.rest;

import gys.pro.fastexam.domain.Expert;
import gys.pro.fastexam.domain.dto.ExpertDTO;
import gys.pro.fastexam.repository.ExpertRepository;
import gys.pro.fastexam.service.ExpertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for Expert entity
 *
 * @author Yegor Gulimov
 */
@RestController
@RequestMapping("/rest/experts")
public class ExpertControllerRest {

    @Autowired
    private ExpertService expertService;

    @RequestMapping("/login/{login}")
    public ResponseEntity getExpertByLogin(@PathVariable("login") String login) {
        Expert expert = expertService.getByLogin(login);
        if (expert == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Expert>(expert, HttpStatus.OK);
    }

    @RequestMapping("/login/{login}/dto")
    public ResponseEntity getExpertDTOByLogin(@PathVariable("login") String login) {
        Expert expert = expertService.getByLogin(login);
        if (expert == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<ExpertDTO>(new ExpertDTO(expert), HttpStatus.OK);
    }


}
