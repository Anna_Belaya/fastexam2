package gys.pro.fastexam;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

/**
 * @author Yegor Gulimov
 */
@ComponentScan(basePackages = { "gys.pro.fastexam" },
    excludeFilters = { @ComponentScan.Filter(type = FilterType.ANNOTATION, value = Configuration.class) })
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class})
public class ApplicationTestConfig { }
