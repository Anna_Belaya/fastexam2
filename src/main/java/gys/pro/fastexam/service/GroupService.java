package gys.pro.fastexam.service;

import gys.pro.fastexam.domain.Group;

import java.util.List;

/**
 * @author Yegor Gulimov
 */
public interface GroupService {
    Group get(String id);
    Group getByName(String name);
    List<Group> list();
    Group create(Group group);
    Group update(String id, Group updated);
    boolean delete(String id);
    void clear();

    List<Group> listByExpert(String expertId);

    Group addExpertToGroup(String groupId, String expertId);
    Group removeExpertFromGroup(String groupId, String expertId);
    Group clearExperts(String groupId);

    Group addStudentToGroup(String groupId, String studentId);
    Group removeStudentFromGroup(String groupId, String studentId);
    Group clearStudents(String groupId);


}
