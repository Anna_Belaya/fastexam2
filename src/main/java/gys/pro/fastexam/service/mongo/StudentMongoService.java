package gys.pro.fastexam.service.mongo;

import gys.pro.fastexam.domain.Student;
import gys.pro.fastexam.repository.StudentRepository;
import gys.pro.fastexam.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Yegor Gulimov
 */

@Service
public class StudentMongoService implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Student create(Student student) {
        try {
            student = studentRepository.insert(student);
        } catch (DuplicateKeyException e) {
            return null;
        }
        return student;
    }

    @Override
    public Student get(String id) {
        return studentRepository.findOne(id);
    }

    @Override
    public List<Student> list() {
        return studentRepository.findAll();
    }

    @Override
    public Student update(String id, Student updated) {
        Student studentActual = studentRepository.findOne(id);
        if (studentActual == null) {
            return null;
        }
        updated.setId(id);
        return studentRepository.save(updated);
    }

    @Override
    public boolean delete(String id) {
        studentRepository.delete(id);
        return !studentRepository.exists(id);
    }

    @Override
    public void clear() {
        studentRepository.deleteAll();
    }
}
