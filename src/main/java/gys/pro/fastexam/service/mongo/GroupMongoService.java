package gys.pro.fastexam.service.mongo;

import gys.pro.fastexam.domain.Expert;
import gys.pro.fastexam.domain.Group;
import gys.pro.fastexam.domain.Student;
import gys.pro.fastexam.repository.ExpertRepository;
import gys.pro.fastexam.repository.GroupRepository;
import gys.pro.fastexam.repository.StudentRepository;
import gys.pro.fastexam.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yegor Gulimov
 */

@Service
public class GroupMongoService implements GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private ExpertRepository expertRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Group get(String id) {
        return groupRepository.findOne(id);
    }

    @Override
    public Group getByName(String name) {
        return groupRepository.findGroupByName(name);
    }

    @Override
    public List<Group> list() {
        return groupRepository.findAll();
    }

    @Override
    public Group create(Group group) {
        if (groupRepository.findGroupByName(group.getName()) != null) {
            return null;
        }
        return groupRepository.insert(group);
    }

    @Override
    public Group update(String id, Group updated) {
        Group actual = groupRepository.findOne(id);
        if (actual == null) {
            return null;
        }

        Group checked = groupRepository.findGroupByName(updated.getName());
        if (checked != null && !checked.getName().equals(actual.getName())) {
            return null;
        }

        updated.setId(id);

        return groupRepository.save(updated);
    }

    @Override
    public boolean delete(String id) {
        if (groupRepository.exists(id)) {
            groupRepository.delete(id);
            return true;
        }
        return false;
    }

    @Override
    public void clear() {
        groupRepository.deleteAll();
    }

    @Override
    public List<Group> listByExpert(String expertId) {
        return groupRepository.findByExpertsId(expertId);
    }

    @Override
    public Group addExpertToGroup(String groupId, String expertId) {
        Group group = groupRepository.findOne(groupId);
        Expert expert = expertRepository.findOne(expertId);
        if (group == null || expert == null) {
            return null;
        }
        List<Expert> groupExperts = group.getExperts();
        if (!groupExperts.contains(expert)) {
            groupExperts.add(expert);
            group.setExperts(groupExperts);
        }
        
        return groupRepository.save(group);
    }

    @Override
    public Group removeExpertFromGroup(String groupId, String expertId) {
        Group group = groupRepository.findOne(groupId);
        Expert expert = expertRepository.findOne(expertId);
        if (group == null || expert == null) {
            return null;
        }
        List<Expert> groupExperts = group.getExperts();
        if (groupExperts.contains(expert)) {
            groupExperts.remove(expert);
            group.setExperts(groupExperts);
        }
        return groupRepository.save(group);
    }

    @Override
    public Group clearExperts(String groupId) {
        Group group = groupRepository.findOne(groupId);
        if (group == null) {
            return null;
        }
        group.setExperts(new ArrayList<Expert>());
        return groupRepository.save(group);
    }

    @Override
    public Group addStudentToGroup(String groupId, String studentId) {
        Group group = groupRepository.findOne(groupId);
        Student student = studentRepository.findOne(studentId);
        if (group == null || student == null) {
            return null;
        }
        List<Student> groupStudents = group.getStudents();
        if (!groupStudents.contains(student)) {
            groupStudents.add(student);
            group.setStudents(groupStudents);
        }

        return groupRepository.save(group);
    }

    @Override
    public Group removeStudentFromGroup(String groupId, String studentId) {
        Group group = groupRepository.findOne(groupId);
        Student student = studentRepository.findOne(studentId);
        if (group == null || student == null) {
            return null;
        }
        List<Student> groupStudents = group.getStudents();
        if (groupStudents.contains(student)) {
            groupStudents.remove(student);
            group.setStudents(groupStudents);
        }
        return groupRepository.save(group);
    }

    @Override
    public Group clearStudents(String groupId) {
        Group group = groupRepository.findOne(groupId);
        if (group == null) {
            return null;
        }
        group.setStudents(new ArrayList<Student>());
        return groupRepository.save(group);
    }

    //getters and setters


    public GroupRepository getGroupRepository() {
        return groupRepository;
    }

    public void setGroupRepository(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public ExpertRepository getExpertRepository() {
        return expertRepository;
    }

    public void setExpertRepository(ExpertRepository expertRepository) {
        this.expertRepository = expertRepository;
    }

    public StudentRepository getStudentRepository() {
        return studentRepository;
    }

    public void setStudentRepository(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
}
