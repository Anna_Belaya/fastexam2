package gys.pro.fastexam.service.mongo;

import gys.pro.fastexam.domain.Expert;
import gys.pro.fastexam.repository.ExpertRepository;
import gys.pro.fastexam.service.ExpertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Yegor Gulimov
 */
@Service
public class ExpertMongoService implements ExpertService {

    @Autowired
    private ExpertRepository expertRepository;

    @Override
    public Expert create(Expert expert) {
        Expert receivedExpert = expertRepository.findExpertByLogin(expert.getLogin()); //unique login
        if (receivedExpert != null) {
            return null;
        }
        return expertRepository.insert(expert);
    }

    @Override
    public Expert get(String id) {
        return expertRepository.findOne(id);
    }

    @Override
    public Expert getByLogin(String login) {
        return expertRepository.findExpertByLogin(login);
    }

    @Override
    public List<Expert> list() {
        return expertRepository.findAll();
    }

    @Override
    public Expert update(String id, Expert updated) {
        Expert expertActual = expertRepository.findOne(id);
        if (expertActual == null) {
            return null;
        }
        Expert expertCheck = expertRepository.findExpertByLogin(updated.getLogin());
        if (expertCheck != null && !expertCheck.getLogin().equals(expertActual.getLogin())) {
            return null;
        }
        updated.setId(id);
        return expertRepository.save(updated);
    }

    @Override
    public boolean delete(String id) {
        expertRepository.delete(id);
        return !expertRepository.exists(id);
    }

    @Override
    public void clear() {
        expertRepository.deleteAll();
    }

    //getters and setters

    public ExpertRepository getExpertRepository() {
        return expertRepository;
    }

    public void setExpertRepository(ExpertRepository expertRepository) {
        this.expertRepository = expertRepository;
    }
}
