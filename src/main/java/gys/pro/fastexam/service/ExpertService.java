package gys.pro.fastexam.service;

import gys.pro.fastexam.domain.Expert;

import java.util.List;

/**
 * @author Yegor Gulimov
 */
public interface ExpertService {
    Expert create(Expert expert);
    Expert get(String id);
    Expert getByLogin(String login);
    List<Expert> list();
    Expert update(String id, Expert updated);
    boolean delete(String id);
    void clear();
}
