package gys.pro.fastexam.service;

import gys.pro.fastexam.domain.Student;

import java.util.List;

/**
 * @author Yegor Gulimov
 */
public interface StudentService {
    Student create(Student student);
    Student get(String id);
    List<Student> list();
    Student update(String id, Student updated);
    boolean delete(String id);
    void clear();
}
